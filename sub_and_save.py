import json
import pandas as pd
import paho.mqtt.client as mqtt
import os

# Configuration
MQTT_BROKER = "127.0.0.1"
MQTT_PORT = 1883
MQTT_TOPIC = "in"
CSV_FILE = "gps_data.csv"

def ddm_to_dd(degrees_minutes, is_longitude=False):
    """
    Convert coordinates from Degrees and Decimal Minutes (DDMM.MMMM or DDDMM.MMMM) format to Decimal Degrees (DD).
    
    Args:
    degrees_minutes (str): Coordinate in DDMM.MMMM or DDDMM.MMMM format.
    is_longitude (bool): Flag indicating whether the input is longitude (default is False, which means latitude).
    
    Returns:
    float: Coordinate in Decimal Degrees.
    """
    # Determine the degree part length
    degree_length = 3 if is_longitude else 2
    
    try:
        # Extract degrees and minutes
        degrees_str = degrees_minutes[:degree_length]
        minutes_str = degrees_minutes[degree_length:]
        
        # Convert degrees part
        degrees = float(degrees_str) if degrees_str else 0
        
        # Convert minutes part
        minutes = float(minutes_str) if minutes_str else 0
        
        # Convert to decimal degrees
        decimal_degrees = degrees + (minutes / 60)
        return decimal_degrees
    except Exception as e:
        print(f"Error converting coordinate '{degrees_minutes}': {e}")
        return None

# Callback when the client receives a connection response from the server
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    # Subscribe to the topic
    client.subscribe(MQTT_TOPIC)

# Callback when a PUBLISH message is received from the server
def on_message(client, userdata, msg):
    try:
        # Decode the message payload
        payload = msg.payload.decode()
        print(f"Received message: {payload}")

        msg_data = json.loads(msg.payload.decode())
        if msg_data['sensor'] == 'geo':

            latitude = ddm_to_dd(str(msg_data['data']['lat']))
            longitude = ddm_to_dd(str(msg_data['data']['lon']))

            # Check if the CSV file exists
            if os.path.exists(CSV_FILE):
                # Load existing data
                df = pd.read_csv(CSV_FILE)
            else:
                # Create a new DataFrame if the file doesn't exist
                df = pd.DataFrame(columns=['latitude', 'longitude'])

            # Append the new data
            if latitude > 0:
                new_data = pd.DataFrame({'latitude': [latitude], 'longitude': [longitude]})
                df = pd.concat([df, new_data], ignore_index=True)

                # Save the updated DataFrame back to CSV
                df.to_csv(CSV_FILE, index=False)

            print("Data saved to CSV")
    except Exception as e:
        print(f"Error processing message: {e}")

# Create an MQTT client instance
client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION1)

# Assign the on_connect and on_message callback functions
client.on_connect = on_connect
client.on_message = on_message

# Connect to the MQTT broker
client.connect(MQTT_BROKER, MQTT_PORT, 60)

# Start the MQTT client loop to process network traffic and dispatch callbacks
client.loop_forever()
