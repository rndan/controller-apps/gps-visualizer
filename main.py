from flask import Flask, render_template
import pandas as pd
import folium

app = Flask(__name__)

@app.route('/')
def index():
    # Load GPS data
    gps_data = pd.read_csv('gps_data.csv')

    # Check if there is any data
    if gps_data.empty:
        # If no data, center the map on a default location (e.g., center of a city)
        map_center = [40.7128, -74.0060]  # Example: New York City coordinates
        zoom_start = 10  # Adjust default zoom level as needed
    else:
        # Center the map around the last recorded GPS coordinate
        last_spot = [gps_data['latitude'].iloc[-1], gps_data['longitude'].iloc[-1]]
        map_center = last_spot
        zoom_start = 10  # Set a lower zoom level initially

    # Create the map with the calculated center and zoom level
    m = folium.Map(location=map_center, zoom_start=zoom_start)

    # Create a list of coordinates from the GPS data
    coordinates = gps_data[['latitude', 'longitude']].values.tolist()

    # Add the polyline to the map
    folium.PolyLine(locations=coordinates, color='blue', weight=5).add_to(m)

    # Add a marker at the last GPS coordinate
    if not gps_data.empty:
        folium.Marker(location=last_spot, popup='Last Spot', icon=folium.Icon(color='red')).add_to(m)

    # Fit the map to the bounds of the polyline with padding
    if not gps_data.empty:
        m.fit_bounds(m.get_bounds(), padding=(50, 50))

    # Save the map to an HTML string
    map_html = m._repr_html_()

    return render_template('index.html', map_html=map_html)

if __name__ == '__main__':
    app.run(host='127.0.0.1', debug=True)

